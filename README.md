# Mental Wealth

A single tenant learning project which allows tracking personal and daily goals with ease.

Technologies involved: 

    Graphql
    
    Nodejs
    
    Typescript
    
    Reactjs
    
    Gitlab CI/CD
    
Features:

    Ability to view dashboard listing personal goals with a compliance score

    Ability to define new daily goals (day, tasks, day summary)
    
    Ability to define aspects of self and rate them at an individual level, associate them with daily goals.
    
    Ability to rate day and goal progression through web forms
